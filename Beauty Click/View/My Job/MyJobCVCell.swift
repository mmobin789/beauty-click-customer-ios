//
//  MyJobCVCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class MyJobCVCell: UICollectionViewCell {
    
    @IBOutlet weak var mapView: GradientView!
   
    @IBOutlet weak var jobHeading: UILabel!
    @IBOutlet weak var dateNtimeHeading: UILabel!
    @IBOutlet weak var priceHeading: UILabel!
    @IBOutlet weak var locationHeading: UILabel!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var dateNtimeLabel: UILabel!
    @IBOutlet weak var nameHeading: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var viewMapBtn: UIButton!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var status: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
//        mapView.clipsToBounds = true
//        mapView.layer.cornerRadius = 20
//        if #available(iOS 11.0, *) {
//            mapView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
//        } else {
//            mapView.roundCorners([.topLeft, .bottomRight], 15, view: mapView)
//            // Fallback on earlier versions
//        }
        
    }
}
