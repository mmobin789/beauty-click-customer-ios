//
//  MyJobVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift


class MyJobVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NetworkLayerDelegate {
 
    @IBOutlet weak var emptyLbl: UILabel!
    
    @IBOutlet weak var myJobCV: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if Localize.currentLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NetworkLayer.shared.order(providerID: Int(User.shared.user_id)!, delegate: self, showHUD: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return JobModel.shared.newJobsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyJobCVCell
        let res = JobModel.init(dic: (JobModel.shared.newJobsArray[indexPath.row] as! [String : Any]))
        
        cell.price.text! = "Sr. \(res.price)"
        cell.customerName.text! = res.customer
        cell.dateNtimeLabel.text! = res.schedule_time
        cell.status.text! = res.status
        cell.service.text! = res.service
        cell.jobHeading.text = "Job".localized()
        cell.nameHeading.text = "Name".localized()
        cell.priceHeading.text = "Price".localized()
        cell.locationHeading.text = "Location".localized()
        cell.dateNtimeHeading.text = "Date/Time".localized()
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width - 20, height: 168)
    }
    
    @IBAction func onclick_back(_ sender: Any) {
        self.present(Utilities.shared.segue(storyboardID: "Home"), animated: true, completion: nil)
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiOrder {
            
            JobModel.shared.newJobsArray.removeAll()
            if let res = dataArray as? NSArray {
                for (_,value) in res.enumerated() {
                    let value = value as! NSDictionary
                    if value.value(forKey: "status") as! String == "Completed"{
                        JobModel.shared.newJobsArray.append(value)
                    }
                }
                myJobCV.reloadData()
            }else{
                myJobCV.isHidden = true
                emptyLbl.isHidden = false
                emptyLbl.text! = "No data found"
            }
            
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiOrder {
            Utilities.shared.showAlert(title: "Error", message: "Couldn't get new jobs. Please try again later.")
        }
    }

    
    
    //
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 15
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyJobVCCell
//
//
//        return cell
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
