//
//  NewEntryPopupCVCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/17/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol ProviderServiceDelegate {
    func checkBox(for cell: NewEntryPopupCVCell, isOn: Bool)
}

class NewEntryPopupCVCell: UICollectionViewCell, BEMCheckBoxDelegate {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var checkBoxService: BEMCheckBox!
    
    var delegate: ProviderServiceDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkBoxService.delegate = self
        self.checkBoxService.onAnimationType = .bounce
        self.checkBoxService.offAnimationType = .bounce
        self.checkBoxService.on = true
        
    }
    
    @objc func didTap(_ checkBox: BEMCheckBox) {
        print("Checkbox tapped")
        self.delegate?.checkBox(for: self, isOn: checkBox.on)
    }
    
}
