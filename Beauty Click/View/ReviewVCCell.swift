//
//  ReviewVCCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class ReviewVCCell: UITableViewCell {

    @IBOutlet weak var review_stars: FloatRatingView!
    @IBOutlet weak var review_desc: UILabel!
    @IBOutlet weak var name_date: UILabel!
    @IBOutlet weak var reviewTitle: UILabel!
    @IBOutlet weak var userType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.review_stars.delegate = self
        self.review_stars.contentMode = UIViewContentMode.scaleAspectFit
        self.review_stars.maxRating = 5
        self.review_stars.minRating = 0
        self.review_stars.editable = true
        self.review_stars.halfRatings = true
        self.review_stars.floatRatings = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
