//
//  PasswordVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/16/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift

class PasswordVC: UIViewController, NetworkLayerDelegate {

    @IBOutlet weak var currentPassBackView: UIView!
    @IBOutlet weak var newPassBackView: UIView!
    @IBOutlet weak var confirmPassBackView: UIView!
    @IBOutlet weak var currentPassTF: UITextField!
    @IBOutlet weak var newPassTF: UITextField!
    @IBOutlet weak var confirmPassTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        Design.object.bottom_Line(views: currentPassBackView, colorName: "grey")
        Design.object.bottom_Line(views: newPassBackView, colorName: "grey")
        Design.object.bottom_Line(views: confirmPassBackView, colorName: "grey")
        
        if Localize.currentLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiUpdatePassword {
            print("Password Updated")
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiUpdatePassword {
            Utilities.shared.showAlert(title: "Error", message: "Couldn't update password. Please try again later.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
