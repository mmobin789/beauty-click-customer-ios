//
//  ServiceVCCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol MyCellDelegate {
    func checkBox(for cell: ServiceVCCell, isOn: Bool)
}

class ServiceVCCell: UITableViewCell, BEMCheckBoxDelegate {

    
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var serviceView: GradientView!
    @IBOutlet weak var serviceTitle: UILabel!
    
    var delegate: MyCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        serviceView.clipsToBounds = false
//        serviceView.layer.cornerRadius = 20
        self.checkBox.delegate = self
        self.checkBox.onAnimationType = .bounce
        self.checkBox.offAnimationType = .bounce
        self.checkBox.on = true
       // if #available(iOS 11.0, *) {
       //     serviceView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
       // } else {
            //serviceView.roundCorners([.topLeft, .bottomRight], 20)
            // Fallback on earlier versions
       // }
    }

    @objc func didTap(_ checkBox: BEMCheckBox) {
        print("Checkbox tapped")
        self.delegate?.checkBox(for: self, isOn: checkBox.on)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
