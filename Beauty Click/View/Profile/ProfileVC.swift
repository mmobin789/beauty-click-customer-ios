//
//  ProfileVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Kingfisher
import Localize_Swift

class ProfileVC: UIViewController, NetworkLayerDelegate {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var onlineSwitch: UISwitch!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var addphotoBtn: UIButton!
    @IBOutlet weak var usernameHeading: UILabel!
    
    var fileURL:URL?
    static var status = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Localize.currentLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        
        let origImage = UIImage(named: "add_photo")
        userImage.image = origImage?.withRenderingMode(.alwaysTemplate)
        userImage.tintColor = UIColor.init(red: 220/255.0, green: 17/255.0, blue: 120/255.0, alpha: 1)
        
        nameTF.text = User.shared.username
        phoneTF.text = User.shared.phone
        emailTF.text = User.shared.email
        userNameTF.text = User.shared.phone
        
        //stars.rating = Float(User.shared.rating)
//        if User.shared.iqamafile != "" {
//            iqamaImg.kf.setImage(with: URL(string: User.shared.iqamafile))
//            Design.object.view_circle(views: iqamaImg)
//        }
        
        //Design.object.view_circle(views: userImage)
        
        let origImageEdit = UIImage(named: "edit")
        let tintedImageEdit = origImageEdit?.withRenderingMode(.alwaysTemplate)
        editBtn.setImage(tintedImageEdit, for: .normal)
        editBtn.tintColor = UIColor.init(red: 220/255.0, green: 17/255.0, blue: 120/255.0, alpha: 1)
        
        //onlineSwitch.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        Design.object.bottom_Line(views: nameTF, colorName: "grey")
        Design.object.bottom_Line(views: phoneTF, colorName: "grey")
        Design.object.bottom_Line(views: emailTF, colorName: "grey")
        Design.object.bottom_Line(views: userNameTF, colorName: "grey")
        nameTF.isUserInteractionEnabled = false
        phoneTF.isUserInteractionEnabled = false
        emailTF.isUserInteractionEnabled = false
        userNameTF.isUserInteractionEnabled = false
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclick_Edit(_ sender: Any) {
        nameTF.isUserInteractionEnabled = true
        phoneTF.isUserInteractionEnabled = true
        emailTF.isUserInteractionEnabled = true
        userNameTF.isUserInteractionEnabled = true
    }
    
    @IBAction func onclick_save(_ sender: Any) {
        nameTF.isUserInteractionEnabled = false
        phoneTF.isUserInteractionEnabled = false
        emailTF.isUserInteractionEnabled = false
        userNameTF.isUserInteractionEnabled = false
        
        guard let file = fileURL else {
            Utilities.shared.showAlert(title: "Alert!", message: "Please select picture.")
            return
        }
        if file.absoluteString != "" && nameTF.text! != "" && phoneTF.text! != "" {
            NetworkLayer.shared.updateProfile(userID: Int(User.shared.user_id)!, name: nameTF.text!, email: emailTF.text!, phone: phoneTF.text!, status: ProfileVC.status, delegate: self, showHUD: true)
        }else{
            Utilities.shared.showAlert(title: "Alert!", message: "Please fill all the fields.")
        }
        
        
    }
    @IBAction func onclick_back(_ sender: Any) {
        self.present(Utilities.shared.segue(storyboardID: "Home"), animated: true, completion: nil)
    }
    
    
    @IBAction func onclick_Upload(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiStatus {
            if onlineSwitch.isOn {
                print("Status Api is On")
            }else{
                print("Status Api is off")
            }
        }else if api == .eApiUpdateProfile {
            Utilities.shared.showAlert(title: "Congratulation", message: "Status updated successfully.")
        }
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiStatus {
            Utilities.shared.showAlert(title: "Error", message: "Status couldn't update. Please try again later.")
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editImage
        }
       
        picker.dismiss(animated: true, completion: nil)
        
        if let data = UIImagePNGRepresentation(selectedImage!){
            
            self.fileURL = Utilities.shared.getDocumentsDirectory()
            self.fileURL!.appendPathComponent("userimage.png")
            
            
            if FileManager.default.fileExists(atPath: self.fileURL!.path){
                do{
                    try FileManager.default.removeItem(at: fileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: self.fileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            self.userImage.kf.setImage(with: self.fileURL!, options: [.forceRefresh])
            Design.object.view_circle(views: userImage)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
