//
//  TermAndConditionVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/6/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import BEMCheckBox
import Localize_Swift

class TermAndConditionVC: UIViewController, BEMCheckBoxDelegate, NetworkLayerDelegate {

    @IBOutlet weak var checkBox: BEMCheckBox!
    
    @IBOutlet weak var T_n_C: UITextView!
    @IBOutlet weak var TnCArabic: UITextView!
    @IBOutlet weak var agreement: UILabel!
    @IBOutlet weak var acceptAgree: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    
    static var concent = false
    var name = String()
    var email = String()
    var username = String()
    var phone = String()
    var gender = Int()
    var dob = String()
    var password = String()
    var disable = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        acceptAgree.text! = "acceprAgree".localized()
        agreement.text! = "t_c".localized()
        okBtn.setTitle("OK", for: .normal)
        if Localize.currentLanguage() == "en" {
            T_n_C.isHidden = false
            TnCArabic.isHidden = true
        }else{
            T_n_C.isHidden = true
            TnCArabic.isHidden = false
        }
        self.checkBox.delegate = self
        self.checkBox.onAnimationType = .bounce
        self.checkBox.offAnimationType = .bounce
        self.checkBox.boxType = .square
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        TermAndConditionVC.concent = checkBox.on
    }

    
    @IBAction func onclick_ok(_ sender: Any) {
        
        if TermAndConditionVC.concent == true {
            print(User.shared.tempFreeAndProv)
            
                //store signup
                //User.shared.tempCode
                //NetworkLayer.shared.SignupEmployee(language: User.shared.tempLanguage, code: User.shared.tempCode, iqama: iqama, username: username, phone: phone, gender: gender, dob: dob, password: password, catagory: User.shared.tempCatagory, delegate: self, showHUD: true)
            
            let emal = email.replacingOccurrences(of: "@", with: "_")
          
            NetworkLayer.shared.Signup(language: User.shared.tempLanguage, username: username, name: name, phone: phone, gender: gender, dob: dob, password: password, email: emal, disable: disable, delegate: self, showHUD: true)
            
            
        }else{
            Utilities.shared.showAlert(title: "Error", message: "Please accept to User's Agreement.")
        }
        
        
    }
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiSignup {
            //print(dataArray)
            // Goto Home VC
            let resp = dataArray as! [String:Any]
            let arr = resp["user"] as! [String : Any]
            let user = User.init(dic: arr)
            User.shared.saveUser(user: user)
            NotificationCenter.default.post(name: .t_n_c, object: self)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiSignup {
            self.dismiss(animated: true, completion: nil)
            Utilities.shared.showAlert(title: "Error", message: "Unable to signup. Please try again later")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
