//
//  FilterVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/11/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import DLRadioButton

class FilterVC: UIViewController, GMSMapViewDelegate, SSRadioButtonControllerDelegate {

    @IBOutlet weak var mapview: UIView!
    
    var googleMap:GMSMapView!
    let marker = GMSMarker()
    var markers:[GMSMarker] = []
    
    @IBOutlet weak var nearBtn: DLRadioButton!
    @IBOutlet weak var farBtn: UIButton!
    @IBOutlet weak var rankingBtn: UIButton!
    @IBOutlet weak var reviewsBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    //var radioButtonController: SSRadioButtonsController?
    var backgroundColor = UIColor()
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        navigationItem.title = "Filter".localized()
        
//        if User.shared.tempfilter == 1 {
//            print(nearBtn.)
//        }else if User.shared.tempfilter == 2 {
//             print(nearBtn.selected())
//        }else if User.shared.tempfilter == 3 {
//             print(nearBtn.otherButtons)
//        }else{
//             print(nearBtn.selectedButtons())
//        }
        
        
        
//        radioButtonController = SSRadioButtonsController(buttons: farBtn, nearBtn, rankingBtn, reviewsBtn, searchBtn)
//        radioButtonController!.delegate = self
//        radioButtonController!.shouldLetDeSelect = true
        //RadioButton.setValue(<#T##NSObject#>)
        
        // Do any additional setup after loading the view.
    }
   

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        googleMap.delegate = self
    }
    
    func didSelectButton(selectedButton: UIButton?)
    {
        NSLog(" \(String(describing: selectedButton))" )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupMap() {
        //let camera = GMSCameraPosition.camera(withLatitude: LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude, zoom: 5.0)
        
        let camera = GMSCameraPosition.camera(withLatitude: 59.915037 , longitude: 10.751318, zoom: 5.0)
        
        googleMap = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        
        //googleMap.isMyLocationEnabled = true
        
        mapview.addSubview(googleMap)
        //self.view.addSubview(googleMap)
        
        let imageData = UIImagePNGRepresentation(UIImage(named: "icon_location")!)
        
        marker.icon = UIImage(data: imageData!, scale: 3.0)
        marker.title = "My Marker"
        //marker.icon = nil
        //marker.position = CLLocationCoordinate2D(latitude:LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude)
        marker.position = CLLocationCoordinate2D(latitude:59.915037, longitude: 10.751318)
        marker.map = googleMap
        
        markers.append(marker)
        
        let myLocation: CLLocationCoordinate2D = self.markers.first!.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
        for m in markers{
            bounds = bounds.includingCoordinate(m.position)
            
        }
        
    }
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
        }
    }
    
    
    @IBAction func onclick_Near(_ sender: Any) {
        User.shared.tempfilter = 1
        
        
        
    }
    @IBAction func onclick_far(_ sender: Any) {
        User.shared.tempfilter = 2
    }
    @IBAction func onclick_ranking(_ sender: Any) {
        User.shared.tempfilter = 3
    }
    @IBAction func onclick_review(_ sender: Any) {
        User.shared.tempfilter = 4
    }
    @IBAction func onclick_search(_ sender: Any) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
