//
//  EarningVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift


class EarningVC: UIViewController, NetworkLayerDelegate {

    @IBOutlet weak var switchCash: UISwitch!
    @IBOutlet weak var myJobEarnings: UILabel!
    @IBOutlet weak var myTotalEarnings: UILabel!
    @IBOutlet weak var myCancelEarnings: UILabel!
    
    @IBOutlet weak var earningHeading: UILabel!
    @IBOutlet weak var myJobsHeading: UILabel!
    @IBOutlet weak var cancelHeading: UILabel!
    @IBOutlet weak var paymentHeading: UILabel!
    @IBOutlet weak var bankTransferHeading: UILabel!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
       
            earningHeading.text = "Earning capital".localized()
            myJobEarnings.text = "My Jobs".localized()

     
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        NetworkLayer.shared.providerEarning(providerID: 1, delegate: self, showHUD: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiProviderEarning {
            let res = EarningModel.init(dic: dataArray as! [String:Any])
            self.myJobEarnings.text! = "Sr. \(res.myjobsEarnings == "" ? "0.00" : res.myjobsEarnings)"
        }
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiProviderEarning {
            Utilities.shared.showAlert(title: "Error", message: "Could't load earing. Please try again later.")
        }
    }
    
    @IBAction func onclick_Switch(_ sender: Any) {
        
        if switchCash.isOn {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankTransferVC") as! BankTransferVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
