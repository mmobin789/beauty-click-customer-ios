//
//  SettingServiceCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/7/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol SettingCellDelegate {
    func checkBox(for cell: SettingServiceCell, isOn: Bool)
}

class SettingServiceCell: UITableViewCell, BEMCheckBoxDelegate {

    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var allServiceTitle: UILabel!
    
    var delegate: SettingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkBox.delegate = self
        self.checkBox.onAnimationType = .bounce
        self.checkBox.offAnimationType = .bounce
        // Initialization code
    }

    @objc func didTap(_ checkBox: BEMCheckBox) {
        print("Checkbox tapped")
        self.delegate?.checkBox(for: self, isOn: checkBox.on)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
