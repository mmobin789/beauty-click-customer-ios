//
//  AddCardAndPhotoVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/12/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift
import Kingfisher

class AddCardAndPhotoVC: UIViewController {

    @IBOutlet weak var lbl_addPhoto: UILabel!
    @IBOutlet weak var lbl_addVisa: UILabel!
    @IBOutlet weak var nxtBtn: UIButton!
    @IBOutlet weak var logoBackView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var iqamaImage: UIImageView!
    
    var iqamaFileURL:URL?
    var userFileURL:URL?
    var userImagee = false
    var iqamaImagee = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        
        Design.object.border_Function(views: logoBackView, colorName: "yellow", borderWidth: 1.0, alpha: 1.0)
        Design.object.view_circle(views: logoBackView, radius: true)
        if Localize.currentLanguage() == "en" {
            print("English is set")
        }else{
            print("Arabic is set")
        }
        setText()
        
        
        if User.shared.tempPicture != "" {
            userImage.clipsToBounds = true
            userImage.layer.cornerRadius = userImage.frame.width/2
            userImage.kf.setImage(with: URL(string: User.shared.tempPicture))
        }
        
        
        // Do any additional setup after loading the view.
    }

    func setText(){
        lbl_addVisa.text = "Add Visa".localized()
        lbl_addPhoto.text = "Add Photo".localized()
        nxtBtn.setTitle("Next".localized(), for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclick_nextBtn(_ sender: Any) {
        
        print(User.shared.tempIqama)
        print(User.shared.tempPicture)
        
        if User.shared.tempPicture != "" && User.shared.tempIqama != "" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailFieldVC") as! DetailFieldVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Utilities.shared.showAlert(title: "Alert", message: "Please add required picture.")
        }
        
    }
    
    @IBAction func onclick_addPhoto(_ sender: Any) {
        self.iqamaImagee = false
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func onclick_addIqama(_ sender: Any) {
        self.iqamaImagee = true
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddCardAndPhotoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editImage
        }
      
        
        picker.dismiss(animated: true, completion: nil)
        
        if let data = UIImagePNGRepresentation(selectedImage!){
            
            if iqamaImagee == true {
                self.iqamaFileURL = Utilities.shared.getDocumentsDirectory()
                self.iqamaFileURL!.appendPathComponent("iqamaimage.png")
            }else{
                self.userFileURL = Utilities.shared.getDocumentsDirectory()
                self.userFileURL!.appendPathComponent("userimage.png")
            }
            
            if FileManager.default.fileExists(atPath: iqamaImagee == true ? self.iqamaFileURL!.path : self.userFileURL!.path){
                do{
                    try FileManager.default.removeItem(at: iqamaImagee == true ? self.iqamaFileURL! : self.userFileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: iqamaImagee == true ? self.iqamaFileURL! : self.userFileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            if iqamaImagee == true {
                User.shared.tempIqama = self.iqamaFileURL!.absoluteString
                self.iqamaImage.kf.setImage(with: self.iqamaFileURL!, options: [.forceRefresh])
            }else{
                userImage.clipsToBounds = true
                userImage.layer.cornerRadius = userImage.frame.width/2
                User.shared.tempPicture = self.userFileURL!.absoluteString
                self.userImage.kf.setImage(with: self.userFileURL!, options: [.forceRefresh])
            }
            
            
        }
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
