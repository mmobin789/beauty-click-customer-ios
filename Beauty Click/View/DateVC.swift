//
//  DateVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/30/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class DateVC: UIViewController {

    @IBOutlet weak var customPicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    
    let currentDate = NSDate()
    let dateFormatter = DateFormatter()
    var datee = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateBtn.setTitle("Done".localized(), for: .normal)
        
        let color = UIColor(red: 220/255, green: 17/255, blue: 120/255, alpha: 1.0)
        
        customPicker.addTarget(self, action: #selector(datePickerChanged), for: UIControlEvents.valueChanged)
        customPicker.setValue(color, forKey: "textColor")
        customPicker.maximumDate = currentDate as Date
        
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: currentDate as Date)
        monthLabel.text = month
        
        dateFormatter.dateFormat = "dd"
        let date = dateFormatter.string(from: currentDate as Date)
        dateLabel.text = date
        
        dateFormatter.dateFormat = "yyyy"
        let year = dateFormatter.string(from: currentDate as Date)
        yearLabel.text = year
        
        // Do any additional setup after loading the view.
    }


    
    
    @objc func datePickerChanged(sender: UIDatePicker){
        
        dateFormatter.dateStyle = .medium
        //dateFormatter.dateFormat = "MMMM-dd-yyyy"
     
        let myNSDate = Date(timeIntervalSince1970: Double(Int(sender.date.timeIntervalSince1970)))
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: myNSDate)
        datee = "\(result)"
        
        
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: sender.date)
        monthLabel.text = month
        
        dateFormatter.dateFormat = "dd"
        let date = dateFormatter.string(from: sender.date)
        dateLabel.text = date
        
        dateFormatter.dateFormat = "yyyy"
        let year = dateFormatter.string(from: sender.date)
        yearLabel.text = year
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneButton(_ sender: Any) {
         NotificationCenter.default.post(name: .selectDate, object: self)
        dismiss(animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
