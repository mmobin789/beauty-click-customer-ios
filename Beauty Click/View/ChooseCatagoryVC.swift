//
//  ChooseCatagoryVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/6/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class ChooseCatagoryVC: UIViewController {

    var gender = Int()
    
    @IBOutlet weak var saloonBackView: UIView!
    @IBOutlet weak var massageBackView: UIView!
    @IBOutlet weak var bathBackView: UIView!
    @IBOutlet weak var barberBackView: UIView!
    @IBOutlet weak var chooseHeading: UILabel!
    @IBOutlet weak var saloonLbl: UILabel!
    @IBOutlet weak var massageLbl: UILabel!
    @IBOutlet weak var bathLbl: UILabel!
    @IBOutlet weak var barberLbl: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        barberLbl.text! = "barber".localized()
        saloonLbl.text! = "salon".localized()
        massageLbl.text! = "massage".localized()
        bathLbl.text! = "bath".localized()
        
        chooseHeading.text! = "Choose Catagory".localized()
        if gender == 1 {
            saloonBackView.isHidden = false
            barberBackView.isHidden = true
        }else{
            saloonBackView.isHidden = true
            barberBackView.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclick_Barber(_ sender: Any) {
        User.shared.tempCatagory = 1
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onclick_saloon(_ sender: Any) {
        User.shared.tempCatagory = 2
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onclick_massage(_ sender: Any) {
        User.shared.tempCatagory = 3
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onclick_Bath(_ sender: Any) {
        User.shared.tempCatagory = 4
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
