//
//  BankTransferVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/1/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift


class BankTransferVC: UIViewController, NetworkLayerDelegate {

    @IBOutlet weak var titleNameTF: UITextField!
    @IBOutlet weak var accNumTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var swiftCodeTF: UITextField!
    
    @IBOutlet weak var bankDetailsHeading: UILabel!
    @IBOutlet weak var paymentCurrHeading: UILabel!
    @IBOutlet weak var bankNameHeading: UILabel!
    @IBOutlet weak var accountNumberHeading: UILabel!
    @IBOutlet weak var depositAccheading: UILabel!
    @IBOutlet weak var swiftCodeHeading: UILabel!
    @IBOutlet weak var withBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Design.object.bottom_Line(views: titleNameTF, colorName: "black")
        Design.object.bottom_Line(views: accNumTF, colorName: "black")
        Design.object.bottom_Line(views: amountTF, colorName: "black")
        Design.object.bottom_Line(views: swiftCodeTF, colorName: "black")
        
        if Localize.currentLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        bankNameHeading.text = "BANK NAME".localized()
        paymentCurrHeading.text = "Payment currency: SR".localized()
        bankDetailsHeading.text = "Bank Details".localized()
        accountNumberHeading.text = "Account Number".localized()
        depositAccheading.text = "Deposit Account".localized()
        swiftCodeHeading.text = "Swift Code".localized()
        titleNameTF.placeholder = "NAME_P".localized()
        accNumTF.placeholder = "Acc_P".localized()
        amountTF.placeholder = "Amount_P".localized()
        swiftCodeTF.placeholder = "swiftC_P".localized()
        withBtn.setTitle("withdraw".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onclick_withdraw(_ sender: Any) {
        
        if titleNameTF.text! != "" && accNumTF.text! != "" && amountTF.text! != "" && swiftCodeTF.text! != "" {
            NetworkLayer.shared.bank(providerID: 1, bankName: titleNameTF.text! , accountNumber: accNumTF.text!, swiftCode: swiftCodeTF.text!, amount: amountTF.text!, delegate: self, showHUD: true)
        }else{
            Utilities.shared.showAlert(title: "Alert", message: "Please fill all fields.")
        }
        
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiBank {
           
            let actionSheet = UIAlertController(title: "Congratulation", message: "Your payment is transfer successfully.", preferredStyle: .alert)
            actionSheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(actionSheet, animated: true, completion: nil)
            
        }
    }
    
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        Utilities.shared.showAlert(title: "Error", message: "Transaction error. Please try again later.")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
