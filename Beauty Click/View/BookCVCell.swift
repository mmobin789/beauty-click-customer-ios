//
//  BookCVCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/11/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

protocol CvCellDelegate {
    func selectedCell(for cell: BookCVCell, isTag: Int)
}

import UIKit
import DropDown

class BookCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var weelLbl: UILabel!
    
    @IBOutlet weak var pickerBackView: UIView!

    @IBOutlet weak var bookView: UIView!
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var weekBtn: UIButton!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var delegate: CvCellDelegate?
    let dropDown = DropDown()

    override func awakeFromNib() {
        super.awakeFromNib()

        dropDown.width = pickerBackView.frame.width
        dropDown.anchorView = weekBtn
        
        Design.object.border_Function(views: pickerBackView, colorName: "black", borderWidth: 1.0, alpha: 1.0)
        bookView.clipsToBounds = true
        bookView.layer.cornerRadius = 20
//        if #available(iOS 11.0, *) {
//            bookView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
//        } else {
//            bookView.roundCorners([.topLeft, .bottomRight], 20, view: <#UIView#>)
//            // Fallback on earlier versions
//        }
        
    }
    
    @objc func didTap(_ label: UILabel) {
        print("Checkbox tapped")
        self.delegate?.selectedCell(for: self, isTag: label.tag) 
    }
    
}
