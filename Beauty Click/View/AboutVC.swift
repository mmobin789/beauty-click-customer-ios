//
//  AboutVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/7/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift
class AboutVC: UIViewController {

    @IBOutlet weak var arabic: UITextView!
    @IBOutlet weak var english: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "About Us".localized()
        
        if Localize.currentLanguage() == "en" {
            arabic.isHidden = true
            english.isHidden = false
        }else{
            arabic.isHidden = false
            english.isHidden = true
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclick_Back(_ sender: Any) {
        self.present(Utilities.shared.segue(storyboardID: "Home"), animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
