//
//  SettingCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var rightImg: UIImageView!
    @IBOutlet weak var leftImg: UIImageView!
    @IBOutlet weak var settingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
