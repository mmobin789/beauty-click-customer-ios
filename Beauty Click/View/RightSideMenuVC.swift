//
//  RightSideMenuVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/14/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class RightSideMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    //var menuArray = ["Home","New Job","My Jobs","Job History","Working Hours","Profile","Earnings","Settings","About Us"]
    var customer = ["Home","My Jobs","Job History","Profile","Settings","About Us"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text! = User.shared.name
        email.text! = User.shared.email
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RightSideMenuCell
        
        cell.menuLabel.text = customer[indexPath.row].localized()
        cell.rightImg.image = UIImage(named: customer[indexPath.row])
        cell.rightImg.image = cell.rightImg.image!.withRenderingMode(.alwaysTemplate)
        cell.rightImg.tintColor = UIColor.gray
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            //performSegue(withIdentifier: "Home", sender: self)
        }else if(indexPath.row == 1){
            performSegue(withIdentifier: "mJob", sender: self)
        }else if(indexPath.row == 2){
            performSegue(withIdentifier: "jHistory", sender: self)
        }else if(indexPath.row == 3){
            performSegue(withIdentifier: "profile", sender: self)
        }else if(indexPath.row == 4){
            performSegue(withIdentifier: "setting", sender: self)
        }else if(indexPath.row == 5){
            performSegue(withIdentifier: "aboutUs", sender: self)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
