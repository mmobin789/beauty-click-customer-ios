//
//  SignupVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/11/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift

class SignupVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
   
    

    @IBOutlet weak var logoBackView: UIView!
    @IBOutlet weak var languageBackView: UIView!
    @IBOutlet weak var providerBackView: UIView!
    @IBOutlet weak var codeBackView: UIView!
    @IBOutlet weak var rightProviderArrow: UIImageView!
    @IBOutlet weak var leftProviderArrow: UIImageView!
    @IBOutlet weak var rightLanguageArrow: UIImageView!
    @IBOutlet weak var leftLanguageArrow: UIImageView!
  
    
    @IBOutlet weak var pickerBackView: UIView!
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var chooseLang: UILabel!
    @IBOutlet weak var chooseProv: UILabel!
    @IBOutlet weak var enterCodeTF: UITextField!
    @IBOutlet weak var nxtBtn: UIButton!
    
    
    var prov = ["Store".localized(),"Freelancer".localized()]
    var pickerIdentifier = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        
        picker.delegate = self
        picker.dataSource = self

       
        pickerBackView.frame.origin.y = self.view.bounds.height
       
        
        Design.object.border_Function(views: codeBackView, colorName: "black", borderWidth: 1.0, alpha: 1.0)
        Design.object.border_Function(views: providerBackView, colorName: "black", borderWidth: 1.0, alpha: 1.0)
        Design.object.border_Function(views: logoBackView, colorName: "yellow", borderWidth: 1.0, alpha: 1.0)
        Design.object.view_circle(views: logoBackView, radius: true)
        
        setText()
         navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
       
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setText()
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        super.viewWillDisappear(animated)
    }
    
    

    func align() {
        if Localize.currentLanguage() == "en" {
            //chooseLang.text = "English".localized()
            //chooseLang.textAlignment = Constants.alignLeft
            chooseProv.textAlignment = Constants.alignLeft
            enterCodeTF.textAlignment = Constants.alignLeft
            //leftLanguageArrow.isHidden = true
            leftProviderArrow.isHidden = true
            //rightLanguageArrow.isHidden = false
            rightProviderArrow.isHidden = false
        }else{
            //chooseLang.text = "Arabic".localized()
            //leftLanguageArrow.isHidden = false
            leftProviderArrow.isHidden = false
            //rightLanguageArrow.isHidden = true
            rightProviderArrow.isHidden = true
            //chooseLang.textAlignment = Constants.alignRight
            chooseProv.textAlignment = Constants.alignRight
            enterCodeTF.textAlignment = Constants.alignRight
        }
    }
    
    //MARK: Actions
    
    @IBAction func onclick_pickerDone(_ sender: Any) {
        UIView.animate(withDuration: 0.1) {
            self.pickerBackView.frame.origin.y = self.view.bounds.height
        }
    }
    

    @IBAction func onclick_provider(_ sender: Any) {
        picker.reloadAllComponents()
        UIView.animate(withDuration: 0.1) {
            self.pickerBackView.frame.origin.y = self.view.frame.height - self.pickerBackView.frame.height
        }
    }
    
    
   
    
    //MARK: UIPickerDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 1 {
            print("Freelancer")
            chooseProv.text = "Freelancer".localized()
            User.shared.tempFreeAndProv = "Freelancer"
            codeBackView.isHidden = true
        }else{
            print("Store")
            chooseProv.text = "Store".localized()
            User.shared.tempFreeAndProv = "Store"
            codeBackView.isHidden = false
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return prov[row].localized()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
