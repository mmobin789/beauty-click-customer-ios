//
//  JobHistoryCVCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class JobHistoryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var completedHeading: UILabel!
    @IBOutlet weak var priceHeading: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    @IBOutlet weak var ratingStars: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
 
    }
}
