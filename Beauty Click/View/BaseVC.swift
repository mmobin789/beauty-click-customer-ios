//
//  BaseVC.swift
//  Gapper
//
//  Created by Haider Ali on 4/25/18.
//  Copyright © 2018 MacBook. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, NetworkLayerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

 //       let backImage = UIImage(named: "back1")
 //       navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
 //       navigationController?.navigationBar.shadowImage = UIImage()
        
//        self.navigationController?.navigationBar.backIndicatorImage = backImage
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = .clear
//        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.tintColor = Constants.TINT_COLOR

//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.barStyle = .default
        //self.navigationController?.navigationBar.tintColor =  Constants.TINT_COLOR
        //self.navigationController?.navigationBar.barTintColor = UIColor.black
    }

}
