//
//  InstaWebView.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/6/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import WebKit
import Foundation


class InstaWebView: UIViewController, UIWebViewDelegate, NetworkLayerDelegate {

    @IBOutlet weak var depWebView: UIWebView!
    
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    
    
    var name = String()
    var userName = String()
    var picture = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        unSignedRequest()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func unSignedRequest() {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        depWebView.loadRequest(urlRequest)
    }
    
    
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        print("Request String: ",requestURLString)
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            print("Range: ",range)
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        
        NetworkLayer.shared.insta(url: "https://api.instagram.com/v1/users/self/?access_token=\(authToken)", delegate: self, showHUD: true)
        
        
    }
    
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("Request URL: ",request)
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityInd.isHidden = false
        activityInd.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityInd.isHidden = true
        activityInd.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiInstagram {
            print(dataArray)
            let arr = dataArray as! NSDictionary
            let data = arr["data"] as! NSDictionary
            name = data.value(forKey: "full_name") as! String
            picture = data.value(forKey: "profile_picture") as! String
            userName = data.value(forKey: "username") as! String
            NotificationCenter.default.post(name: .instaLogin, object: self)
            dismiss(animated: true, completion: nil)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiInstagram {
            Utilities.shared.showAlert(title: "Error", message: "Couldn't login with instagram.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
