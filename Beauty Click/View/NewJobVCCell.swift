//
//  NewJobVCCell.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class NewJobVCCell: UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var schedualTime: UILabel!
    @IBOutlet weak var orderTime: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var pinkView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        pinkView.layer.cornerRadius = 5.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
