//
//  SideMenu.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/13/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class SideMenu: UIViewController, UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var customer = ["Home","My Jobs","Job History","Profile","Settings","About Us"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text! = User.shared.name
        emailLabel.text! = User.shared.email
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return customer.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuCell
        
        
        cell.menuLabel.text = customer[indexPath.row]
        cell.leftImg.image = UIImage(named: customer[indexPath.row])
        cell.leftImg.image = cell.leftImg.image!.withRenderingMode(.alwaysTemplate)
        cell.leftImg.tintColor = UIColor.gray
        
        
        
        //cell.rightImg.image = UIImage(named: menuArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            //performSegue(withIdentifier: "Home", sender: self)
        }else if(indexPath.row == 1){
            performSegue(withIdentifier: "mJob", sender: self)
        }else if(indexPath.row == 2){
            performSegue(withIdentifier: "jHistory", sender: self)
        }else if(indexPath.row == 3){
            performSegue(withIdentifier: "profile", sender: self)
        }else if(indexPath.row == 4){
            performSegue(withIdentifier: "setting", sender: self)
        }else if(indexPath.row == 5){
            performSegue(withIdentifier: "aboutUs", sender: self)
        }
        
    }

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
