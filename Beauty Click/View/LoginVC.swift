//
//  ViewController.swift
//  Beauty Click
//
//  Created by Arqam Butt on 6/11/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit
import Localize_Swift
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit
import TwitterCore


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NetworkLayerDelegate {

    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var pickerBackView: UIView!
    @IBOutlet weak var NameBackView: UIView!
    @IBOutlet weak var passBackView: UIView!
    @IBOutlet weak var logoBackView: UIView!
   
    @IBOutlet weak var languageBackView: UIView!
    @IBOutlet weak var rightLanguageArrow: UIImageView!
    @IBOutlet weak var leftLanguageArrow: UIImageView!
    @IBOutlet weak var chooseLang: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    
    var lang = ["English".localized(), "عربى"]
    static var langu = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        print(Localize.currentLanguage())
        if Localize.currentLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        Design.object.bottom_Line(views: NameBackView, colorName: "black")
        Design.object.bottom_Line(views: passBackView, colorName: "black")
        Design.object.view_circle(views: logoBackView, radius: true)
        Design.object.border_Function(views: logoBackView, colorName: "yellow", borderWidth: 1.0, alpha: 1.0)
        Design.object.border_Function(views: languageBackView, colorName: "black", borderWidth: 1.0, alpha: 1.0)
        pickerBackView.frame.origin.y = self.view.bounds.height
        setText()
        
        //NetworkLayer.shared.employeeProviderService(providerID: 1, serviceID: 1, isOffered: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.freelancerProviderService(providerID: 1, serviceID: 1, isOffered: 1, price: 20, discount: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.updateProfile(userID: 47, name: "name", email: "abc_gmail.com", phone: "0333", profileImage: "profile.jpg", status: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.addWorkingHours(providerID: 1, day: "Friday", time: "12:00 PM to 9:00 PM", delegate: self, showHUD: true)
        //NetworkLayer.shared.updatePassword(userID: 47, pa"ssword: "new_password", delegate: self, showHUD: true)
        //NetworkLayer.shared.deleteProviderService(providerID: 1, serviceID: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.bank(providerID: 47, bankName: "BankALHabib", accountNumber: "1010", swiftCode: "0066", amount: "10000", delegate: self, showHUD: true)
        //NetworkLayer.shared.language(providerID: 47, language: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.providerEarning(providerID: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.status(userID: 1, status: 1, latitude: 1.00, longitute: 1.00, delegate: self, showHUD: true)
        //NetworkLayer.shared.fcm(userID: 1, fcmToken: "sdsdsdsdsd121212sdssd", delegate: self, showHUD: true)
        //NetworkLayer.shared.order(providerID: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.review(providerID: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.getWorkingHours(providerID: 1, delegate: self, showHUD: true)
        //NetworkLayer.shared.Signup(language: 1, iqama: "iqama", username: "Arqam123", name: "Arqam Butt", phone: "0333", gender: 0, dob: "1992-01-01", password: "12345", iqama_attach: "iqama.png", document_attach: "doc.png", delegate: self, showHUD: true)
        
        
        NotificationCenter.default.addObserver(forName: .instaLogin, object: nil, queue: OperationQueue.main) { (notification) in
            let insta = notification.object as! InstaWebView
            User.shared.tempName = insta.name
            User.shared.tempPicture = insta.picture
            User.shared.tempUsername = insta.userName
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailFieldVC") as! DetailFieldVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.navigationBar.tintColor = Constants.TINT_COLOR
        if User.shared.isLoggedIn() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! SWRevealViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

    
//    let time = "12:00 PM to 9:00 PM"
//    let escapedString = time.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//    print(escapedString!)

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func setText() {
        nameTF.placeholder = "Name".localized()
        passwordTF.placeholder = "Password".localized()
        loginBtn.setTitle("Login".localized(), for: .normal)
        signupBtn.setTitle("Sign Up".localized(), for: .normal)
        align()
    }
    
    func align() {
        if Localize.currentLanguage() == "en" {
            chooseLang.text = "English".localized()
            chooseLang.textAlignment = Constants.alignLeft
            nameTF.textAlignment = Constants.alignLeft
            passwordTF.textAlignment = Constants.alignLeft
            leftLanguageArrow.isHidden = true
            rightLanguageArrow.isHidden = false
        }else{
            chooseLang.text = "Arabic".localized()
            nameTF.textAlignment = Constants.alignRight
            passwordTF.textAlignment = Constants.alignRight
            leftLanguageArrow.isHidden = false
            rightLanguageArrow.isHidden = true
            chooseLang.textAlignment = Constants.alignRight
      
        }
    }
    
    
    @IBAction func onclick_signup(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailFieldVC") as! DetailFieldVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func onclick_Login(_ sender: Any) {
        if nameTF.text != "" && passwordTF.text != "" {
            NetworkLayer.shared.UserLogin(username: nameTF.text!, password: passwordTF.text!, language: ViewController.langu, delegate: self, showHUD: true)
        }else{
            Utilities.shared.showAlert(title: "Error", message: "Please enter Username or Password")
        }
        
    }
    @IBAction func onclick_selectLang(_ sender: Any) {
        picker.reloadAllComponents()
        UIView.animate(withDuration: 0.1) {
            self.pickerBackView.frame.origin.y = self.view.frame.height - self.pickerBackView.frame.height
        }
    }
    @IBAction func onclick_pickerDone(_ sender: Any) {
        UIView.animate(withDuration: 0.1) {
            self.pickerBackView.frame.origin.y = self.view.bounds.height
        }
    }
    
    @IBAction func onclick_FbLogin(_ sender: Any) {
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Login Error ............................................................ \(err!)")
                Utilities.shared.showAlert(title: "Error", message: "\(err!)")
                return
            }else{
                if (FBSDKAccessToken.current() != nil) {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name,email,picture"]).start { (connection, result, error) in
                        if error != nil {
                            print("Graph Request Error............................................................ \(error!)")
                            Utilities.shared.showAlert(title: "Error", message: "\(error!)")
                            return
                        }else{
                            if let data = result  {
                                let user = data as! NSDictionary
                                print(user)
                                let email = String(describing: user.value(forKey: "email")!)
                                //let id = String(describing: user.value(forKey: "id")!)
                                let name = String(describing: user.value(forKey: "name")!)
                                User.shared.tempName = name
                                User.shared.tempEmail = email

                                let pic = user["picture"] as! NSDictionary
                                let data = pic["data"] as! NSDictionary
                                let photo = String(describing: data.value(forKey: "url")!)
                                User.shared.tempPicture = photo
                                
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailFieldVC") as! DetailFieldVC
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                
                            }
                        }
                    }
                }else{
                    print("FB Token Error .........................................")
                    Utilities.shared.showAlert(title: "Error", message: "Try again later.")
                    return
                }
            }
        }
        
        
    }
    @IBAction func onclick_insta(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstaWebView") as! InstaWebView
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onclick_Twitter(_ sender: Any) {
        
        Twitter.sharedInstance().logIn { (session, error) -> Void in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");

                User.shared.tempUsername =  session!.userName
    
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailFieldVC") as! DetailFieldVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        }
        
    }
    
    
    
    //MARK: UIPickerDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 1 {
            print("Arabic")
            ViewController.langu = 2
            User.shared.tempLanguage = 2
            chooseLang.text = "Arabic".localized()
            Localize.setCurrentLanguage("ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            print("English")
            ViewController.langu = 1
            User.shared.tempLanguage = 1
            chooseLang.text = "English".localized()
            Localize.setCurrentLanguage("en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        UserDefaults.standard.set(true, forKey: Constants.UserSelectedLanguage)
        UserDefaults.standard.synchronize()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return lang[row].localized()
    }
    
    //MARK: Network Layer Delegate
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {

        if api == .eApiLogin {
            let resp = dataArray as! [String:Any]
            let arr = resp["user"] as! [String : Any]
            //print(arr)
            let user = User.init(dic: arr)
            User.shared.saveUser(user: user)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! SWRevealViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiLogin {
            Utilities.shared.showAlert(title: "Error", message: "Invalid Username or password.")
        }
    }
}

