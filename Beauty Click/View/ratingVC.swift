//
//  ratingVC.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/15/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import UIKit

class ratingVC: UIViewController, FloatRatingViewDelegate {

    @IBOutlet weak var rateBackView: UIView!
    @IBOutlet weak var reviewTF: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var rating: FloatRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        img.image = UIImage(named: "review")
        img.image = img.image!.withRenderingMode(.alwaysTemplate)
        img.tintColor = Constants.TINT_COLOR
        
        Design.object.bottom_Line(views: rateBackView, colorName: "black")
        
        self.rating.delegate = self
        self.rating.contentMode = UIViewContentMode.scaleAspectFit
        self.rating.maxRating = 5
        self.rating.minRating = 0
        self.rating.editable = true
        self.rating.halfRatings = true
        self.rating.floatRatings = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclick_ok(_ sender: Any) {
        
    }
    
    //MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        print(rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Float) {
        print(rating)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
