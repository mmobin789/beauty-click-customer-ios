//
//  InstaModel.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/6/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import Foundation

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "f844e895f3a64ab7843b9a70ef9b732a"
    
    static let INSTAGRAM_CLIENTSERCRET = "494dcc7436e049d9b41572e05dc3253b"
    
    static let INSTAGRAM_REDIRECT_URI = "https://www.Beautyclickk.com/home"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

