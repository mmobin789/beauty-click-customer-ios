//
//  JobModel.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/7/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import Foundation
import ObjectMapper

class JobModel: Mappable {
    
    static let shared = JobModel()
    
    var customer = String()
    var customer_id = String()
    var order_id = String()
    var order_time = String()
    var price = String()
    var rating = String()
    var schedule_time = String()
    var service = String()
    var service_id = String()
    var status = String()
    var tax = String()
    var tax_rate = String()
    var tax_id = String()
    var user_type = String()
    var user_id = String()
    var username = String()
    
    var newJobsArray = [Any]()
    
    
    init() {}
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        customer            <- map["customer"]
        customer_id         <- map["customer_id"]
        order_id            <- map["order id"]
        order_time          <- map["order_time"]
        price               <- map["price"]
        rating              <- map["rating"]
        schedule_time       <- map["schedule_time"]
        service             <- map["service"]
        service_id          <- map["service_id"]
        status              <- map["status"]
        tax                 <- map["tax"]
        tax_rate            <- map["tax rate"]
        tax_id              <- map["tax_id"]
        user_type           <- map["user type"]
        user_id             <- map["user_id"]
        username            <- map["username"]
        
    }
    
}
