//
//  WorkingModel.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/2/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import Foundation
import ObjectMapper

class WorkingModel: Mappable {
    
    static let shared = WorkingModel()
    
    var user_id = String()
    var day = String()
    var from = String()
    var name_eng = String()
    var to = String()
    var working_hours_id = String()
    var workingHourArray = [Any]()
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        name_eng             <- map["name_eng"]
        from                 <- map["from"]
        day                  <- map["day"]
        to                   <- map["to"]
        user_id              <- map["user_id"]
        working_hours_id     <- map["working hours id"]
        
    }
    
}
