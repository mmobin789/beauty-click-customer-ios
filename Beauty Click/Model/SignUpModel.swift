//
//  SignUpModel.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/11/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import Foundation
import ObjectMapper

class SignUp: Mappable {
    
    var id = "0"
    var name = String()
    var email = String()
    var gender = String()
    var language_id = String()
    var username = String()
   
    
    static let shared = SignUp()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    
    
    
    func deleteUser() {
        id = ""
        name = ""
        email = ""
        gender = ""
        username = ""
        language_id = ""
        saveUser(user: self)
    }
    
    func stringID() -> String{
        return "\(id)"
    }
    
    func loadUser() {
        let userDef = UserDefaults.standard
        if ((userDef.string(forKey: Constants.USER_DATA)) != nil) {
            let uString = UserDefaults.standard.value(forKey: Constants.USER_DATA) as! String
            let mapper = Mapper<SignUp>()
            let userObj = mapper.map(JSONString: uString)
            let map = Map.init(mappingType: .fromJSON, JSON: (userObj?.toJSON())!)
            self.mapping(map:map)
        }
    }
    
    func saveUser(user:SignUp) {
        UserDefaults.standard.set(user.toJSONString()!, forKey: Constants.USER_DATA)
        UserDefaults.standard.synchronize()
        loadUser()
    }
    
    func isLoggedIn() -> Bool {
        return id > "0" ? true : false
    }
    
    // Mappable
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        email                   <- map["email"]
        gender                  <- map["gender"]
        username                <- map["username"]
        language_id             <- map["language_id"]
    }
    
}
