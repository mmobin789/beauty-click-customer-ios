//
//  StoreModel.swift
//  Beauty Click
//
//  Created by Arqam Butt on 7/12/18.
//  Copyright © 2018 Arqam Butt. All rights reserved.
//

import Foundation
import ObjectMapper

class StoreModel: Mappable {
    
    static let shared = StoreModel()
    
    var user_id = String()
    var name = String()
    var minprice = String()
    var profession = String()
    var dailyTimings = String()
    var discount = String()
    var image = String()
    var storeArray = [Any]()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        user_id           <- map ["user_id"]
        profession        <- map["profession"]
        name              <- map["name"]
        minprice          <- map["minprice"]
        dailyTimings      <- map["dailyTimings"]
        discount          <- map["discount"]
        image             <- map["image"]
    }
    
}
